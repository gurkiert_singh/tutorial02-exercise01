﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace week02_ex01
{
    class Program
    {
        static void Main(string[] args)
        {
            var number1 = 3;
            var number2 = 5;

            if (number1 > number2)
            {
                Console.WriteLine("Number 1 is bigger than Number 2");
            }
            else
                Console.WriteLine("Number 1 smaller or equal to Number 2");
        }
    }
}
